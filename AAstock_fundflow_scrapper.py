import os
from datetime import datetime

from bs4 import BeautifulSoup
import requests
import pandas as pd
import numpy as np
from apscheduler.schedulers.background import BlockingScheduler
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger
from apscheduler.triggers.cron import CronTrigger


class aastock_fundflow_scrap:

    def __init__(self, folder_directory):
        self.folder = folder_directory

    def __save_df_to_path(self, df, date, minute=False):
        if not os.path.exists(f'{self.folder}'):
            os.makedirs(f'{self.folder}', exist_ok=True)

        if not minute:
            if not os.path.exists(f"{self.folder}/{date}_AAstocks_fundflow_30days_data.csv"):
                df.to_csv(f"{self.folder}/{date}_AAstocks_fundflow_30days_data.csv", index=True, encoding='utf_8_sig')
            else:
                print(f'The corresponding {date} data file already exists in your folder')

        if minute:
            if not os.path.exists(f"{self.folder}/{date}_AAstocks_fundflow_minute_data.csv"):
                df.to_csv(f"{self.folder}/{date}_AAstocks_fundflow_minute_data.csv", index=True, encoding='utf_8_sig')
            else:
                print(f'The corresponding {date} data file already exists in your folder')

    def __duplicate(self, testList, n):
     return [ele for ele in testList for _ in range(n)]

    def aastock_scraping(self, Save = True):
        #南向合計
        url = 'http://www.aastocks.com/tc/cnhk/market/quota-balance/hk-connect'
        r = requests.get(url)
        soup = BeautifulSoup(r.text,'html.parser')
        column_headers = []
        aggr_column_names = []
        aggr_Date = []
        aggr_Daily_Quota_Balance = []
        aggr_Money_Flow = []
        aggr_Buy_Trade_Value = []
        aggr_Sell_Trade_Value = []
        aggr_Total_Trade_Value = []

        table = soup.find('table',id='tabSec')
        for name in table.find_all('th'):
            aggr_column_names.append(name.text)
        table_rows =soup.find_all('tr',class_='bord2 txt_c')
        for row in table_rows:
            data = row.find_all('td')
            aggr_Date.append(data[0].text)
            aggr_Daily_Quota_Balance.append(data[1].text)
            aggr_Money_Flow.append(data[2].text)
            aggr_Buy_Trade_Value.append(data[3].text)
            aggr_Sell_Trade_Value.append(data[4].text)
            aggr_Total_Trade_Value.append(data[5].text)
        column_headers.insert(0,aggr_column_names.pop(0))
        aggr_column_names[5]='總成交額(佔大市成交%)'

        #滬港通南向
        url_2 = 'http://www.aastocks.com/tc/cnhk/market/quota-balance/sh-hk-connect'
        r_2 = requests.get(url_2)
        soup_2 = BeautifulSoup(r_2.text,'html.parser')
        sh_column_names = []
        sh_Daily_Quota_Balance = []
        sh_Money_Flow = []
        sh_Buy_Trade_Value = []
        sh_Sell_Trade_Value = []
        sh_Total_Trade_Value = []

        table_2 = soup_2.find('table', id='tabSec')
        for name_2 in table_2.find_all('th'):
            sh_column_names.append(name_2.text)
        table_2_rows = soup_2.find_all('tr',class_='bord2 txt_c')
        for row in table_2_rows:
            data = row.find_all('td')
            sh_Daily_Quota_Balance.append(data[1].text)
            sh_Money_Flow.append(data[2].text)
            sh_Buy_Trade_Value.append(data[3].text)
            sh_Sell_Trade_Value.append(data[4].text)
            sh_Total_Trade_Value.append(data[5].text)
        column_headers.insert(1,sh_column_names.pop(0))
        sh_column_names=aggr_column_names[1:]

        #深港通南向
        url_3='http://www.aastocks.com/tc/cnhk/market/quota-balance/sz-hk-connect'
        r_3 = requests.get(url_3)
        soup_3 = BeautifulSoup(r_3.text,'html.parser')
        sz_column_names = []
        sz_Daily_Quota_Balance = []
        sz_Money_Flow = []
        sz_Buy_Trade_Value = []
        sz_Sell_Trade_Value = []
        sz_Total_Trade_Value = []

        table_3 = soup_3.find('table', id='tabSec')
        for name_3 in table_3.find_all('th'):
            sz_column_names.append(name_3.text)
        table_3_rows = soup_3.find_all('tr',class_='bord2 txt_c')
        for row in table_3_rows:
            data = row.find_all('td')
            sz_Daily_Quota_Balance.append(data[1].text)
            sz_Money_Flow.append(data[2].text)
            sz_Buy_Trade_Value.append(data[3].text)
            sz_Sell_Trade_Value.append(data[4].text)
            sz_Total_Trade_Value.append(data[5].text)
        column_headers.insert(1,sz_column_names.pop(0))
        sz_column_names=aggr_column_names[1:]

        #Integration
        column_headers = self.__duplicate(column_headers,5)
        aggr_column_names.remove('日期')
        all_column_names = aggr_column_names+sh_column_names+sz_column_names
        tuples = zip(column_headers,all_column_names)
        multicol = pd.MultiIndex.from_tuples(tuple(tuples))
        frame = np.array([aggr_Daily_Quota_Balance,aggr_Money_Flow,aggr_Buy_Trade_Value,aggr_Sell_Trade_Value,aggr_Total_Trade_Value,
                         sh_Daily_Quota_Balance,sh_Money_Flow,sh_Buy_Trade_Value,sh_Sell_Trade_Value,sh_Total_Trade_Value,
                         sz_Daily_Quota_Balance,sz_Money_Flow,sz_Buy_Trade_Value,sz_Sell_Trade_Value,sz_Total_Trade_Value]).transpose()
        df = pd.DataFrame(frame,columns=multicol,index=aggr_Date)
        if Save:
            self.__save_df_to_path(df,date=str(aggr_Date[0]).replace('/','_'))
        if not Save:
            return df

    def aastock_regular_scraping(self, Save = True):
        scheduler = BlockingScheduler()
        intervalTrigger = CronTrigger(hour=16,minute=15,second=0)
        scheduler.add_job(lambda:self.aastock_scraping(Save=Save),
                          intervalTrigger, id='regular scrapper')
        scheduler.start()

    def aastock_minute_regular_scraping(self,df:list):
        main_scheduler = BlockingScheduler()
        cronTrigger = CronTrigger(hour=9,minute=30,second=0)
        main_scheduler.add_job(lambda:self.__aastock_minute_interval_scraping(df),
                          cronTrigger, id='minute regular scrapper')
        main_scheduler.start()

    def __row_appending(self,df:list,scheduler:BlockingScheduler):
        df[0] = df[0].append(self.aastock_scraping(Save=False).iloc[0],ignore_index=True)
        if datetime.now().hour == 16 and datetime.now().minute == 15:
            self.__save_df_to_path(df[0],date=str(self.aastock_scraping(Save=False).index[0]).replace('/','_'),minute=True)
            df[0] = pd.DataFrame(columns=self.aastock_scraping(Save=False).columns)
            scheduler.shutdown(wait=False)

    def __aastock_minute_interval_scraping(self,df:list):
        sub_scheduler = BlockingScheduler()
        intervalTrigger = IntervalTrigger(minutes=1)
        sub_scheduler.add_job(lambda:self.__row_appending(df,scheduler=sub_scheduler),
                          intervalTrigger, id='row appending')
        sub_scheduler.start()

#Try
scrap = aastock_fundflow_scrap(r'C:\Users\cheun\Documents\aastock_scrap')
scrap.aastock_minute_regular_scraping(df=[pd.DataFrame(columns=scrap.aastock_scraping(Save=False).columns)])
